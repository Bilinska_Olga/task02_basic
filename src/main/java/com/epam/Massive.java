package com.epam;

import static java.lang.Double.MAX_VALUE;

public class Massive {
    static void fibo(int a, int b, int c) {
        int f3;
        int[] fiboArr = new int[c + 2];
        fiboArr[0] = a;
        fiboArr[1] = b;


        for (int i = a, x = 2; i <= c; i++, x++) {
            f3 = a + b;
            fiboArr[x] = f3;
            a = b;
            b = f3;
        }
        System.out.print(" ");
        for (int element : fiboArr) {
            System.out.print(element + " ");
        }

        //вивід парних чисел//
        int even = 0;
        int odd = 1;

        for (int i = 0; i < fiboArr.length; i++) {
            if (fiboArr[i] % 2 == 0) {
                even++;
            }
        }

        //розрахунок відсотка парних чисел
        int evenPercent = (even * 100) / fiboArr.length;
        System.out.println("Відсоток парних чисел: " + evenPercent + "%");

        //вивід непарних чисел
        for (int i = 0; i < fiboArr.length; i++) {
            if (fiboArr[i] % 2 != 0) {
                odd++;
            }
        }

        //розрахунок відсотка непарних чисел
        int oddPercent = (odd * 100) / fiboArr.length;
        System.out.println("Відсоток непарних чисел: " + oddPercent + "%");


        int max1 = fiboArr[0];
        int max2 = fiboArr[1];

        // провірка в масиві
        for (int i = 0; i < fiboArr.length; i++) {
            // порівняння значень з масива парних цифр з елементами перемінних
            if (fiboArr[i] % 2 == 0 && fiboArr[i] > max1) {
                max1 = fiboArr[i];
            }
        }
        for (int i = 0; i < fiboArr.length; i++) {
            if (fiboArr[i] % 2 != 0 && fiboArr [i] > max2) {
                max2 = fiboArr[i];
            }
        }
        // вивід на екран макс знач з масива
        System.out.println("Максимальне непарне число: " + max2);
        System.out.println("Максимальне парне число: " + max1);
    }
}







