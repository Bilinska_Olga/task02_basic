package com.epam;

import java.util.Arrays;
import java.util.Scanner;

public class Generalf {
    public static void main(String[] args) {
        Scanner num = new Scanner(System.in);
        int f1;      /*значення початку інтервалу*/
        int f2;      /*значення закінчення інтервалу*/

        /*користувача вносить інтервал*/
        System.out.println("Введіть число початку інтервалу від 1 до 99");
        f1 = num.nextInt();
        System.out.println("Введіть число закінчення інтервалу від 2 до 100");
        f2 = num.nextInt();

        /*підрахунок непарних чисел */
        System.out.println("Непарні числа з Вашого інтервалу:");
        /*змінна в яку будемо вносити сумму непарних чисел */
        int sumOdd = 0;
        int i = f1;
        while (i < f2) {
            i += 1;
            if (i % 2 == 0) {
                continue;
            }
            System.out.print(i + " ");
            sumOdd += i;

        }
        System.out.println("");
        System.out.println("Сумма непарних чисел: " + sumOdd);
        System.out.println("");

        /*підрахунок парних чисел */
        System.out.println("Парні числа з Вашого інтервалу:");
        int sumEven = 0;    /*змінна в яку будемо вносити сумму парних чисел */
        int e = f1;
        while (e < f2) {
            e += 1;
            if (e % 2 != 0) {
                continue;
            }
            System.out.print(e + " ");
            sumEven += e;
        }
        System.out.println("");
        System.out.println("Сумма парних чисел: " + sumEven);
        System.out.println("");

        /*користувач вносит довжину фібоначі*/
        Scanner length = new Scanner(System.in);
        int n;
        System.out.println("Вивід розмір порядку Фебоначчі");
        n = length.nextInt() - 2;
        Massive.fibo(f1, f2, n);  /*виклик методу фібо з классу фібоначі*/
    }
    }











